#!/usr/bin/env bash
#abyss preprocess pipeline configuration file

##############################################
#MAIN PARAMETERS
##############################################

#Paths to data and references
DATAREF=TO_BE_SET
#DATAREF=/home/ref-bioinfo/tools_data/path/to/reference/databases
DATAWORK=TO_BE_SET
#DATAWORK=/home/datawork-merlinabyss/path/to/pipeline/output

#Locus to be analysed (16S-fuhrman or 16S-archaea, COI or 18S-V1 18S-V4 or 18S-V9)
export locus=18S-V9
echo "Barcode locus = $locus"

# OUTPUT DIR WITH RESULTS
export output_dir=$DATAWORK/abyss-pipeline-outputs
echo "Output directory for Abyss pipeline = $output_dir"
# For raw ligation data (primer-removal included):
export ligation_preprocess_dir=${output_dir}/$locus/1_ligation_preprocess
# For raw double-PCR data (primer-removal included):
export cutadapt_dir=${output_dir}/${locus}/1_cutadapt
export cleaning_dir=${output_dir}/${locus}/1_R1R2_cleaned
# For all data:
export dada_dir=${output_dir}/${locus}/2_dada
export blast_dir=${output_dir}/${locus}/3_blast
export FROGS_dir=${output_dir}/${locus}/4_frogs
export extract_amp_dir=${output_dir}/${locus}/5_extract_amplicons
# Processes for final curation after data refining:
export tag_switching_dir=${output_dir}/${locus}/6_tag_switching
export lulu_dir=${output_dir}/${locus}/7_lulu

# INPUT DIR WITH DATA TO PROCESS
export input_dir=TO_BE_SET
echo "Input directory with raw sequencing data = ${input_dir}"

# dada2 merge ASV tables located in a specific input directory
# Warning : input_merge is the folder containing the ASV tables to merge (file names = name_asv.rds)
export input_merge=TO_BE_SET
echo "If merging ASV tables from different runs, dada2mergeasv inputs are in ${input_merge}"
# in case you run dada2mergeasv AND NOT planning to do clustering after:
#export output_merge=$input_merge
# or specify new dada_dir
#export output_merge=$dada_dir
# in case you run dada2mergeasv AND planning to do clustering after:
export output_merge=$FROGS_dir
echo "If merging ASV tables from different runs, dada2mergeasv outputs are in ${output_merge}"

# PIPELINE MAIN DIRECTORY
#Path to abyss-pipeline local git cloned repository
export main_dir=$DATAWORK/abyss-pipeline

#pbs scripts dir
export pbs_dir=$main_dir/pbs
#echo "PBS scripts directory = $pbs_dir"

#R python and sh scripts 
export scripts_dir=$main_dir/commun
#echo "Common scripts directory = $scripts_dir"

# reference databases folder
export ref_dir=$DATAREF
#echo "Reference databases directory = $ref_dir"

#######################################################
##PIPELINE STEPS TO RUN
#######################################################
#abyss-preprocessing pipeline for raw ligation data (primer-removal included)
export ligation_preprocess=yes

#cleaning (primer removal and reads sorting for double-PCR data)
export cleaning_process=no

#dada2
export dada_main_process=yes
# dada2-merge process only : ASV tables to merge are in the folder input_merge, the result will be written in folder output_merge
export dada_mergeasv_process=no

# blast assignation of ASVs (out of frogs)
export blast_assignation=yes

#frogs_process: to be used for preprocessing+DADA2+FROGS
export frogs_process=yes
export extract_amplicons=no
# frogs_only: to be used for preprocessing+FROGS
# if ligation_preprocess=no and cleaning_process=no, set frogs_archive to the path of the frogs input tar archive (samples have to have primers removed)
export frogs_only=no
export frogs_archive=/PATH/TO/FROGS/TAR

# Following steps must be run alone!!!
#tag switching normalisation
export tag_switching_norm_process=no

#lulu curation
export lulu_process=no

#######################################################
##LIGATION PREPROCESS
#######################################################
# input data are ligation-based  (R1F/R2R and R2F/R1R)
export ligation=yes

if [ ${ligation_preprocess} == "yes" ]; then
    export ligation_config_file=${main_dir}/thirdparty/abyss-preprocessing/extract.ini
    # remove primers from data
    export trim=True
fi

#######################################################
#DADA MAIN PROCESS
# lengthMin, lengthMax are amplicon size ranges (no primers)
# rdpbank=taxonomic reference database for RDP in DADA2
# blastbank=taxonomic reference database for blast_assignation process
#######################################################
if [ ${dada_main_process} == "yes" ] || [ ${blast_assignation} == "yes" ]; then
    export script_dada=${scripts_dir}/dada2main.R
    export script_dadamerge=${scripts_dir}/dada2mergeasv.R
    export script_dada2output=${scripts_dir}/dada2outputfiles.R
    if [ "$locus" = "COI" ]; then
	    export lengthMin=300
        export lengthMax=326
		# MIDORI-marine (GB v.224) DADA2-formatted RDP database (incl. abyss mock COI sequences)
        export rdpbank=$ref_dir/dada2/MIDORI_UNIQUE_20180221_COI_wo_terrestrials_rdp_formatted.fasta
        # MIDORI-UNIQUE (GB v.224) marine-only database (incl. abyss mock COI sequences)
        export blastbank=$ref_dir/frogs/midori/20190813/MIDORI_UNIQUE_COI_MARINE_20180221/w_abyssal/MIDORI_UNIQUE_COI_MARINE_20180221_w_abyssal_rdp_formatted.fasta
        # MIDORI-LONGEST (GB v.242)
        #export blastbank="/home/datawork-merlinabyss/4_Abyssal_diversity/1_dna_ref_databases/midori-GB242/MIDORI_LONGEST_SP_GB242_CO1_RDP.fasta"
    elif [ "$locus" = "16S-fuhrman" ]; then
        export lengthMin=350
        export lengthMax=390
        export blastbank=$ref_dir/frogs/16S_silva138/silva_138.1_16S_pintail80.fasta
        export rdpbank=$ref_dir/dada2/silva_nr_v132_train_set.fa
    elif [ "$locus" = "16S-archaea" ]; then
        export lengthMin=350
        export lengthMax=390
        export blastbank=$ref_dir/frogs/16S_silva138/silva_138.1_16S_pintail80.fasta
        export rdpbank=$ref_dir/dada2/silva_nr_v132_train_set.fa
    elif [ "$locus" = "18S-V1" ]; then
        export lengthMin=330
        export lengthMax=390
        #Silva 18S db + abyss 18S sequences
        export blastbank=$ref_dir/frogs/18S_silva138_abyss-seq_animals/silva_138.1_18S_mod_mock_95seqs.fasta
        export rdpbank=$ref_dir/dada2/silva_nr_v132_train_set.fa
    elif [ "$locus" = "18S-V4" ]; then
        export lengthMin=350
        export lengthMax=410
        #Silva 18S db + abyss 18S sequences
        export blastbank=$ref_dir/frogs/18S_silva138_abyss-seq_animals/silva_138.1_18S_mod_mock_95seqs.fasta
        export rdpbank=$ref_dir/dada2/silva_nr_v132_train_set.fa
    elif [ "$locus" = "18S-V9" ]; then
        export lengthMin=87
        export lengthMax=186
        export blastbank=$ref_dir/frogs/pr2-4.13.0/pr2-4.13.0_18S_RDP.fasta
		export rdpbank=$ref_dir/dada2/pr2_version_4.13.0_18S_dada2.fasta.gz
    fi
fi

#######################################################
#DADA MERGE ASV
#######################################################
if [ ${dada_mergeasv_process} == "yes" ]; then
    export script_dadamerge=${scripts_dir}/dada2mergeasv.R
fi

#######################################################
# FROGS
# Banque=taxonomic reference database
# lengthMin, lengthMax are amplicon size ranges (no primers)
# R1size, R2size: Paired-end sequencing lengths - primer lengths (only for frogs_only process)
# expectedAmpliconSize: expected amplicon size (no primers, only for frogs_only process)
# range: this is the d-value for clustering
#######################################################
if [ ${frogs_process} == "yes" ] || [ ${frogs_only} == "yes" ]; then
    
    if [ ${extract_amplicons} = "yes" ]; then
     	export script_ext_amp=${scripts_dir}/extract_amplicons.sh
    fi
    if [ ${locus} = "16S-fuhrman" ]; then
        export banque=$ref_dir/frogs/16S_silva138/silva_138.1_16S_pintail80.fasta
        export lengthMin=350 
        export lengthMax=390
        export R1size=TO_BE_SET 
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=1 #$(seq 1 7) 
    elif [ ${locus} = "16S-archaea" ]; then
        export banque=$ref_dir/frogs/16S_silva138/silva_138.1_16S_pintail80.fasta
        export lengthMin=350
        export lengthMax=390
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=1
    elif [ ${locus} = "18S-V1" ]; then
        # Silva w/wo abyssal barcodes: 
		#export banque=$ref_dir/frogs/18S_silva138/silva_138.1_18S.fasta
		export banque=$ref_dir/frogs/18S_silva138_abyss-seq_animals/silva_138.1_18S_mod_mock_95seqs.fasta
        # marine protists PR2 Roscoff
        #export banque=$ref_dir/frogs/pr2-4.13.0/pr2-4.13.0_18S_RDP.fasta
        export lengthMin=330
        export lengthMax=390
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=1
    elif [ ${locus} = "18S-V4" ]; then
		# Silva w/ abyssal barcodes:
        export banque=$ref_dir/frogs/18S_silva138_abyss-seq_animals/silva_138.1_18S_mod_mock_95seqs.fasta
        # marine protists PR2 Roscoff
        #export banque=$ref_dir/frogs/pr2-4.13.0/pr2-4.13.0_18S_RDP.fasta
        export lengthMin=350
        export lengthMax=410
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=1
    elif [ ${locus} = "18S-V9" ]; then
		# Silva w/ abyssal barcodes:
        #export banque=$ref_dir/frogs/18S_silva138_abyss-seq_animals/silva_138.1_18S_mod_mock_95seqs.fasta
        # marine protists PR2 Roscoff
        export banque=$ref_dir/frogs/pr2-4.13.0/pr2-4.13.0_18S_RDP.fasta
        # alternative mix 16S/18S of silva132
        #export banque=$ref_dir/frogs/mix_18S16S_silva132_animals/mix_18S16S_silva132.fasta
        export lengthMin=85
        export lengthMax=190
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=1
    elif [ ${locus} = "COI" ]; then
        # MIDORI-marine database (GenBank v.224 from 2018) + abyss mock COI sequences
        #export banque=$ref_dir/frogs/midori/20190813/MIDORI_UNIQUE_COI_MARINE_20180221/w_abyssal/MIDORI_UNIQUE_COI_MARINE_20180221_w_abyssal_rdp_formatted.fasta
		# MIDORI-LONGEST full database based on GB v.242:
        export banque="/home/datawork-merlinabyss/4_Abyssal_diversity/1_dna_ref_databases/midori-GB242/MIDORI_LONGEST_SP_GB242_CO1_RDP.fasta"
        export lengthMin=300
        export lengthMax=326
        export R1size=TO_BE_SET
        export R2size=TO_BE_SET
        export expectedAmpliconSize=TO_BE_SET
        export range=13
    fi
fi

#######################################################
#TAG SWITCHING NORMALIZATION
#######################################################
if [ ${tag_switching_norm_process} == "yes" ]; then
    export script_owi_renormalize=${scripts_dir}/owi_renormalize.R
    export tag_switching_cutoff=0.97
    #Path to the tsv count table to normalize (probably : refined_MOTU_tables/10_ASVs_counts_refined.tsv)
    export input_table_tag_switching=TO_BE_SET
    #Colstart is the first column of the tsv table with count data (ie. first sample) and colend is the last column containing count data (ie : the last sample)
    export colstart=TO_BE_SET #col1 = clusterID, is supposed to begin at 2
    export colend=TO_BE_SET #is supposed to be ncol or(sample number)+1
fi

#######################################################
#LULU curation
#######################################################
if [ ${lulu_process} == "yes" ]; then
    export script_lulu=${scripts_dir}/lulu.R
    # Parametres Lulu : identity threshold
    export identite_lulu=84
    # Parametres Lulu : cooccurence threshold
    export cooccurence_lulu=0.95
    #Path to the renormalised count tsv table to curate, OTUs in rows with first column being ClusterID (probably : renormalised_MOTU_table/ASVs_counts_renormalised.tsv)
    export input_table_lulu=TO_BE_SET
    #Path to the fasta of ClusterIDs to curate (probably : LULU_curation_input/10_ASVs_refined.fasta)
    export input_fasta_lulu=TO_BE_SET
    #Path to the taxonomy table to merge in the end (probably : LULU_curation_input/10_ASVs_taxonomy_refined.fasta)
    export input_taxonomy_lulu=TO_BE_SET
fi

#######################################################
#Double PCR data preprocess
#######################################################
if [ "$ligation" = "no" ] ; then
    # Sample names is like : Sample_name.[R1format].fastq.gz
    # R1format and R2format has to be set below : 
    export R1format="R1" #stands for Sample_name.R1.fastq.gz
    export R2format="R2"
    #Cutadapt parameters according to locus:
	#primer sequences, mismatches authorized (error rate adjusted to allow 3 nt mismatch, except for COI=7nt, and overlap is primer length minus 1)
    if [ "$locus" = "18S-V1" ]; then
        
		export primerF="-g GCTTGTCTCAAAGATTAAGCC"
        export primerR="-g CCTGCTGCCTTCCTTRGA"
        export error_rateF=0.15
        export error_rateR=0.17
        export overlapF=20
        export overlapR=17
        
    elif [ "$locus" = "18S-V4" ]; then
        
        export primerF="-g CCAGCASCYGCGGTAATTCC"
        export primerR="-g ACTTTCGTTCTTGATYRA"
        export error_rateF=0.15
        export error_rateR=0.17
        export overlapF=20
        export overlapR=17
        
    elif [ "$locus" = "18S-V9" ]; then
        
        export primerF="-g TTGTACACACCGCCC"
        export primerR="-g CCTTCYGCAGGTTCACCTAC"
        export error_rateF=0.15
        export error_rateR=0.17
        export overlapF=20
        export overlapR=17
        
    elif [ "$locus" = "16S-fuhrman" ]; then
        	
       	export primerF="-g GTGYCAGCMGCCGCGGTAA"
        export primerR="-g CCGYCAATTYMTTTRAGTTT"
        export error_rateF=0.17
        export error_rateR=0.15
        export overlapF=18
        export overlapR=19
        
    elif [ "$locus" = "16S-archaea" ]; then
        	
       	export primerF="-g GCCTAAAGCATCCGTAGC -g GCCTAAARCGTYCGTAGC -g GTCTAAAGGGTCYGTAGC -g GCTTAAAGNGTYCGTAGC -g GTCTAAARCGYYCGTAGC"
        export primerR="-g CCGGCGTTGANTCCAATT"
        export error_rateF=0.17
        export error_rateR=0.15
        export overlapF=18
        export overlapR=19
        
    else
	#COI:
       	export primerF="-g GGWACWGGWTGAACWGTWTAYCCYCC"
        export primerR="-g TANACYTCNGGRTGNCCRAARAAYCA"
        export error_rateF=0.27
        export error_rateR=0.27
        export overlapF=25
        export overlapR=25
    fi
fi
