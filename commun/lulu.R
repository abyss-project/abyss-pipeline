#!/usr/bin/env Rscript

library(lulu)

# Recuperer les arguments passes au programme
args = commandArgs(trailingOnly=TRUE)

# Table d'OTU a traiter
otu_table = args[1]

# Match list pour le nettoyage
match_list = args[2]

# minimum_match threshold:
identite_lulu = args[3]

# LULU output directory
out_dir = args[4]

# LULU output file
out_rds = args[5]

# LULU output table
out_tsv = args[6]

#tax table
tax_table = args[7]

# minimum cooccurence threshold
cooccurence = args[8]

print("Read the count and taxonomy tables")
table_OTU_complete  <- read.csv(otu_table,sep='\t',header=TRUE,as.is=TRUE)
rownames(table_OTU_complete) <- table_OTU_complete$ClusterID
table_OTU_complete$ClusterID <- NULL
tax_table_complete <- read.csv(tax_table,sep='\t',header=TRUE,as.is=TRUE)
print("Read the matchlist")
matchlist <- read.table(match_list, header=FALSE,as.is=TRUE, stringsAsFactors=FALSE)

# Executer Lulu
print("Execute Lulu")
curated_result <- lulu(as.data.frame(table_OTU_complete), matchlist, minimum_ratio_type = "min", minimum_ratio = 1, minimum_match = identite_lulu, minimum_relative_cooccurence = cooccurence)
print(curated_result)
# Ajout des colonnes de taxonomy
print("Adding taxonomy columns")
df <- data.frame(curated_result$curated_otus,curated_result$curated_table)
colnames(df)[1] = "ClusterID"
print("Merge lulu with taxonomy table")
lulu_curated_result <- merge(df, as.data.frame(tax_table_complete), by="ClusterID")
write.table(lulu_curated_result, paste0(out_dir,"/",out_tsv), sep = "\t", row.names = F)

# Sauvegarder les resultats dans un fichier (c'est juste au cas ou un truc planterait dans la suite, ce serait con de devoir refaire Lulu!)
saveRDS(curated_result, paste0(out_dir,"/",out_rds))
