# MERLIN Abyss metabarcoding pipeline

All scripts for abyss metabarcoding pipeline.
Gitlab Repositories : 
- https://gitlab.ifremer.fr/abyss-project/abyss-pipeline
- https://gitlab.ifremer.fr/abyss-project/abyss-preprocessing (submodule of abyss-pipeline, can be run alone or inside abyss-pipeline)

### Get the pipeline locally
You can use this command to clone your repo with all the submodules:
- git clone --recursive https://gitlab.ifremer.fr/abyss-project/abyss-pipeline.git

Or, if you have already cloned the project and forgot the --recursive option, you can use:
- git submodule update --init --recursive

## Pipeline steps
**(optional: use abyss-rename to rename sequence files before running abyss-pipeline)**

abyss-pipeline steps are :

- Ligation data preprocessing i.e. ligation_preprocess: 
    - classify and clean R1F/R2R R2F/R1R reads with cutadapt
    - sort reads with bbmap repair

OR

- Double PCR data preprocessing i.e. cleaning process: 
    - Cutadapt : remove adapters/primers for R1/R2 reads
    - Cleaning : keep only valid couples for R1/R2 reads with bbmap repair

THEN

- Dada2 (Callahan et al. 2016) : read correction and production of Amplicon Sequence Variants (ASVs), Taxonomy assignment with RDP Classifier
- Blast : Blast taxonomy assignation for Amplicon Sequence Variants (ASVs), merged to RDP taxonomy table

THEN (OPTIONAL)

- FROGS : clustering with swarm v2 (Mahe et al. 2015) / additional chimera removal / affiliation of Operational Taxonomic Units (OTUs) with RDP and BLAST (Escudie et al. 2017)

THEN

- Tag switching correction : sort the samples by abundance of each ASV/OTU and eliminate the reads of the samples corresponding to a cumulative frequency of less than 3% for each particular ASV/OTU (Wangensteen & Turon 2016, Metabarpark project)
- Lulu : ASV/OTU table curation based on similarity and co-occurence rates (Froslev et al. 2017)

frogs_only steps are :

- Preprocess reads either using FROGS, or abyss-pipeline (Ligation data preprocessing or Double PCR data preprocessing, see above) and produce a tar archive of all preprocessed samples

- Process cleaned samples of tar archive via FROGS pipeline without DADA2 denoising using frogs.pbs

## Getting Started

### Configuration file

The example configuration file is (you can copy/paste this file to create several scenarios) :
```
config.sh
```

Modify the following lines :

- locus : locus targeted by input files (possibles values : 16S-fuhrman, 16S-archaea, COI, 18S-V1, 18S-V4, 18S-V9)
- output_dir : main output directory (supposed to be on Datawork server)
- main_dir : path to your own copy of abyss-pipeline project when "git clone" from the gitlab

Directories :

- input_dir : input directory with fastq.gz files (supposed to be on Dataref data server) OR in case of recovery run and abyss-preprocessing (ligation), set the input directory to the wanted previous step (e.g : ${ligation_preprocess_dir})
- pbs_dir : path to pbs script in the pipeline (pbs)
- scripts_dir : path to python, R and bash scripts in the pipeline (common)
- ref_dir : path to reference databases for taxonomic assignation (likely also on dataref)

Steps to activate for running the pipeline (**recovery runs are possible**):

**(only for ligation-based, i.e. R1FR2R/R2FR1R data)**
- ligation_preprocess :
    - set ligation to **yes** if input data has been produced by adaptor ligation
    - set trim=True if you want cutadapt to remove primers from data
    - ligation_config_file : path to extract.ini file (configuration file of abyss-preprocessing pipeline), do not change path.
        - properties.ini file with primer sequences for each locus and allowed mismatches
        - extract.ini file is config file for preprocessing, do not change if run within abyss-pipeline:  
```
#Input parameters
[common]
#Do we process trimreads ?
trimreads = %(trim)s
#Below configuration is optimized to use abyss-preprocessing inside abyss-pipeline
#if run alone, see the alternative configuration in the comments
directory = %(main_dir)s/thirdparty/abyss-preprocessing
#if run alone : directory=PATH_TO_GIT_CLONE_OF_ABYSS_PREPROCESSING
#file containing primer definitions and selected barcode
properties = %(main_dir)s/thirdparty/abyss-preprocessing/properties.ini
#if run alone : properties=FULL_PATH_TO_PROPERTIES.INI_FILE
barcode = %(locus)s
#if run alone : barcode=18-V1 for exemple
#input directory with raw data
indir = %(input_dir)s
#if run alone : indir=PATH_TO_INPUT_FASTQ_DIRECTORY
#output directory with processed reads
output = %(ligation_preprocess_dir)s
#if run alone : ouput=PATH_TO_OUTPUT_DIRECTORY

```
**(only for regular double PCR paired-end data)**
- cleaning_process : 
    - will activate Double PCR data preprocess with cutadapt for removing primer sequences and production of data archive for frogs_only
    - Double-check parameters in Double PCR data preprocess:
        - R1format and R2format?
        - Cutadapt parameters correct?

**(for all data)**
- dada_main_process : to run dada2 denoising on samples
    - if needed, update dada2 parameters in scripts_dir/dada2main.R
- dada_mergeasv_process : to merge several ASV tables created by several runs and produce output files of merged data
    - can be run alone on a directory containing files named: NAME_asv.rds
    - input_merge : path to the directory withi NAME_asv.rds tables to merge
    - output_merge : path to the folder to contain the final merged ASV table (usually $dada_dir or $FROGS_dir, if clustering afterwards)
- blast_assignation : to run BLAST assignment of ASVs after dada2, blast databases are preconfigured for each locus in config.sh
- frogs_process : to run FROGS pipeline on dada2 output, including swarm clustering, additional chimera removal, RDP/blast assignation
- extract_amplicons : after swarm clustering, extract and align all amplicon sequences of each OTU
- frogs_only : to run FROGS pipeline with swarm clustering, chimera removal, RDP/blast assignation **without dada2 denoising**
- frogs_archive : if ONLY frogs_only is set to yes, then indicate location of raw samples (no primers/adaptors) in tar archive and fill out FROGS parameters in config. If you want FROGS to remove adaptors/primers, then you will need to modify your version of the frogs.pbs script.
    -   **Adjust BLAST assignement criteria** in $pbs_dir/blast.pbs for ASVs and $scripts_dir/affiliation_OTU_identite_couverture.py for OTUs
    
**Steps to run after data refining in R:**

- tag_switching_norm_process : to run tag switching normalization, see Wangensteen & Turon 2016 for explanation (Metabarpark)
    - tag_switching_cutoff at 97% by default
    - input_table_tag_switching : to be set manually, ASV/OTU table containing read counts
    - colstart and colend values must be set manually to frame count data columns in count table (usually colstart=2 and colend=nbsamples-1)
- lulu_process : to run LULU curation of ASVs/OTUs
    - identite_lulu is minimum_match parameter,, default is 84%, to adjust depending on marker gene
    - cooccurence_lulu is minimum_relative_cooccurence parameter, default is 0.95 (95%)
    - minimum_ratio and minimum_ratio_type parameters have to be updated in scripts_dir/lulu.R
    - input files have to be separate counts (with first column being ClusterID), fasta, and taxonomy files (containing ClusterID column)


For more details about the pipeline configuration, check config.sh

### Running the pipeline

Once your config.sh file is ready, run the main.sh script :

```
./main.sh -f myconfig.sh
```
This will run several pbs jobs for each step of the pipeline. In order to check the process on Datarmor, run this commands :
```
qstat -u LOGIN
```
To get the details on array jobs (jobnumber[].datarmor0) :
```
qstat -t jobnumber[].datarmor0
```
