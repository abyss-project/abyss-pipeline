#!/usr/bin/env bash
#abyss preprocess pipeline
usage() { echo "Usage : $0 [-f <config_file.sh>]" 1>&2; exit 1; }

while getopts ":f:" option; do
    case "${option}" in
        f)config=${OPTARG};;
        *)usage ;;
    esac
done
if [ -z "${config}" ]; then
	usage
fi

#Load config file
echo "#############################################"
echo "READ CONFIGURATION FILE"
echo "#############################################"
echo "Configuration file = $config"
. $config
#create output directory
mkdir -p ${output_dir}/$locus/logs
echo "Creating output dir : ${output_dir}/$locus"

CLEANINGJOBS=""	
##############################################
##      EXCECUTING EACH OF THE PROCESSES     ##
##############################################
## handle ligation data
    if [ ${ligation} == "yes" ]; then
        echo "LIGATION = YES"
        if [ ${ligation_preprocess} == "yes" ]; then
            echo "Running abyss-preprocessing pipeline for preprocessing raw ligation data"
            mkdir -p ${ligation_preprocess_dir}
            . /appli/bioinfo/python/3.6/env.sh
            LIGATION=$(python ${main_dir}/thirdparty/abyss-preprocessing/extract.py --config-file ${ligation_config_file})
            . /appli/bioinfo/python/3.6/delenv.sh
        fi
    else 
        echo "LIGATION = NO"
        ## handle classic R1/R2 data
        if [ ${cleaning_process} == "yes" ]; then
            # CUTADAPT
            #############################################
            #run cutadapt jobs
            echo "Submitting cutadapt jobs"
            
            #create output dir
            mkdir -p ${cutadapt_dir}
            mkdir -p ${cleaning_dir}
            #handle each couple R1/R2
            COUPLES=0
            for R1 in ${input_dir}/*$R1format*; do
                export COUPLES=$(($COUPLES + 1))
                R2=$(echo $R1 | sed 's/'$R1format'/'$R2format'/g')
                echo "R1=$R1"
                echo "R2=$R2"
                # link rename each file for process through arrayjob
				ln -s $R1 ${cutadapt_dir}/R1@$COUPLES.fastq.gz
                ln -s $R2 ${cutadapt_dir}/R2@$COUPLES.fastq.gz
                CUTADAPT=$(qsub -o $output_dir/$locus/logs/cutadaptPBSoutput-$COUPLES -V ${pbs_dir}/cutadapt.pbs)
                echo "Cutadapt job number : $CUTADAPT"
                  
                # link rename each file for process through arrayjob
                CLEANING=$(qsub -o $output_dir/$locus/logs/cleaningPBSoutput-$COUPLES -W depend=afterok:$CUTADAPT -V ${pbs_dir}/bbmap.pbs)
                #concatenate dependency jobs for dada2 or frogs
                IFS="." read -r -a JOBNUM <<< "$CLEANING"
                CLEANINGJOBS="$CLEANINGJOBS:${JOBNUM[0]}"
                echo "CLEANING JOBS = $CLEANINGJOBS"
            done
            #create archive for frogs
            ARCHIVE=$(qsub -o $output_dir/$locus/logs/createFrogsArchive.log -W depend=afterok$CLEANINGJOBS -V ${pbs_dir}/createfrogsarchiveR1R2.pbs) 
            echo "Archive = $ARCHIVE"
        fi
    fi

# DADA2
#############################################
if [ ${dada_main_process} == "yes" ]; then
    echo "Submitting dada job"
    #create output dir
    mkdir -p ${dada_dir}
    if [ ${ligation} == "yes" ]; then
        if [ ${ligation_preprocess} == "yes" ]; then
	    export R1name="R1F"
            export R2name="R2R"
            DADA1=$(qsub -o $output_dir/$locus/logs/dada2main${R1name}${R2name}.log -W depend=afterok:$LIGATION -V ${pbs_dir}/dada2.pbs)
            export R1name="R2F"
            export R2name="R1R"
            DADA2=$(qsub -o $output_dir/$locus/logs/dada2main${R1name}${R2name}.log -W depend=afterok:$LIGATION -V ${pbs_dir}/dada2.pbs)
        else
            export R1name="R1F"
            export R2name="R2R"
            DADA1=$(qsub -o $output_dir/$locus/logs/dada2main${R1name}${R2name}.log -V ${pbs_dir}/dada2.pbs)
            export R1name="R2F"
            export R2name="R1R"
            DADA2=$(qsub -o $output_dir/$locus/logs/dada2main${R1name}${R2name}.log -V ${pbs_dir}/dada2.pbs)
        fi
        # merge ligation dada outputs
        DADAMERGE=$(qsub -o $output_dir/$locus/logs/dada2merged.log -W depend=afterany:$DADA1:$DADA2 -V ${pbs_dir}/dada2mergeasv.pbs)
        DADAOUTPUT=$(qsub -o $output_dir/$locus/logs/dada2outputfiles.log -W depend=afterok:$DADAMERGE -V ${pbs_dir}/dada2outputfiles.pbs)
    else
        export R1name=$R1format
        export R2name=$R2format
        if [ $CLEANINGJOBS != "" ]; then
            DADA=$(qsub -o $output_dir/$locus/logs/dada2main.log -W depend=afterok$CLEANINGJOBS -V ${pbs_dir}/dada2.pbs)
            DADAOUTPUT=$(qsub -o $output_dir/$locus/logs/dada2outputfiles.log -W depend=afterok:$DADA -V ${pbs_dir}/dada2outputfiles.pbs)
        else
           echo "Cannot wait for cutadapt and cleaning jobs : jobs id = $CLEANINGJOBS" 
        fi
    fi
    echo "Dada job number : $DADA $DADAMERGE $DADAOUTPUT"
    echo "#############################################"
fi

if [ ${dada_mergeasv_process} == "yes" ]; then
    echo "Submitting dada merging job"
    mkdir -p ${output_merge}
    DADAMERGE=$(qsub -o $output_dir/$locus/logs/dada2merged.log -V ${pbs_dir}/dada2mergeasv.pbs)
    DADAOUTPUT=$(qsub -o $output_dir/$locus/logs/dada2outputfiles.log -W depend=afterok:$DADAMERGE -V ${pbs_dir}/dada2outputfiles.pbs)
	echo "Dada merge asv job number : $DADAMERGE $DADAOUTPUT"
	echo "#############################################"
fi	

# BLAST assignation of ASVs
#############################################

if [ ${blast_assignation} == "yes" ]; then
	# make output dir
	mkdir -p ${blast_dir}
	if [ ${dada_main_process} == "yes" ] || [ ${dada_mergeasv_process} == "yes" ]; then
        echo "Perform blast assignation after Dada2 process"
        BLAST=$(qsub -o $output_dir/$locus/logs/blast_assignation.log -W depend=afterok:$DADAOUTPUT -V ${pbs_dir}/blast.pbs)
        CONCAT=$(qsub -o $output_dir/$locus/logs/concat_rdp_blast_tax.log -W depend=afterok:$BLAST -V ${pbs_dir}/concat_blast_rdp_tax.pbs)
	else
        echo "Perform blast assignation on ${dada_dir}/10_ASVs.fasta"
        BLAST=$(qsub -o $output_dir/$locus/logs/blast_assignation.log -V ${pbs_dir}/blast.pbs)
        CONCAT=$(qsub -o $output_dir/$locus/logs/concat_rdp_blast_tax.log -W depend=afterok:$BLAST -V ${pbs_dir}/concat_blast_rdp_tax.pbs)
    fi
	echo "Blast job number : $BLAST"
    echo "Concat job number : $CONCAT"
	echo "#############################################"
fi
	
# FROGS
#############################################
echo "ligation=${ligation}"
echo "ligation_preprocess=${ligation_preprocess}"
echo "cleaning_process=${cleaning_process}"
echo "frogs_only=${frogs_only}"
echo "dada_main_process=${dada_main_process}"
echo "dada_mergeasv_process=${dada_mergeasv_process}"
echo "frogs_process=${frogs_process}"

if [ ${frogs_only} == "yes" ]; then
	echo "Submitting frogs-only job"
	#create output dir
    mkdir -p ${FROGS_dir}
	if [ ${ligation_preprocess} == "yes" ]; then
        #run only frogs for ligation data
        echo "Run only frogs on ligation data"
        FROGS=$(qsub -o $output_dir/$locus/logs/frogs.log -W depend=afterok:$LIGATION -V ${pbs_dir}/frogs.pbs)
    elif [ ${cleaning_process} == "yes" ];then
        echo "Run only frogs on double PCR data after cleaning process"
        FROGS=$(qsub -o $output_dir/$locus/logs/frogs.log -W depend=afterok:$ARCHIVE -V ${pbs_dir}/frogs.pbs)
    elif [ -f ${frogs_archive} ]; then
		# frogs must be run on .tar archive specified in config-file
        echo "Running frogs alone on the archive ${frogs_archive}"
        FROGS=$(qsub -o $output_dir/$locus/logs/frogs.log -V ${pbs_dir}/frogs.pbs)
        exit 0
	else 
        echo "${frogs_archive} does not exist" 
    fi
	echo "frogs job number : $FROGS"
    echo "#############################################"
fi    

if [ ${frogs_process} == "yes" ]; then
    echo "Submitting frogs clustering job"
    #create output dir
    mkdir -p ${FROGS_dir}
    # frogs must be run on dada2 results (dada2+swarm)
    if [ ${dada_main_process} == "yes" ] || [ ${dada_mergeasv_process} == "yes" ]; then 
        echo "Run frogs clustering after dada process on dada results"
        FROGS=$(qsub -o $output_dir/$locus/logs/frogs.log -W depend=afterok:$DADAOUTPUT -V ${pbs_dir}/frogs.pbs)
    else
		#start frogs clustering on dada data that has already been produced
        echo "Run frogs clustering on already produced dada output"
        FROGS=$(qsub -o $output_dir/$locus/logs/frogs.log -V ${pbs_dir}/frogs.pbs)
	fi
    echo "frogs job number : $FROGS"
    echo "#############################################"
fi

# LULU
##############################################
if [ ${lulu_process} == "yes" ] ; then
    #create output dir
    mkdir -p ${lulu_dir}
    LULU=$(qsub -o $output_dir/$locus/logs/lulu.log -V ${pbs_dir}/lulu.pbs)
    echo "Lulu job number : $LULU"
    echo "#############################################"
fi

# TAG SWITCHING NORMALIZATION
##############################################
if [ ${tag_switching_norm_process} == "yes" ] ; then
    #create output dir
    mkdir -p ${tag_switching_dir}
    TS=$(qsub -o $output_dir/$locus/logs/tag_switching.log -V ${pbs_dir}/tag_switching_norm.pbs)
    echo "TAG Switching normalization process : $TS"
    echo "#############################################"
fi


whoami=`whoami`
echo "type : 'qstat -u $whoami' to control pipeline execution"
echo "pbs logs are in $output_dir/$locus/logs directory"
echo "pipeline outputs are in $output_dir/$locus"
