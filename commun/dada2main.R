#!/usr/bin/env Rscript

library(plyr)
library(dplyr)
library(tidyr)
library(dada2)
library(ggplot2)

basicdada <- function(path, dadapath, lengthMin, lengthMax, patternR1, patternR2, locus){
    # Chemin vers les R1-R2 sans primers
    fnFs <- sort(list.files(path, pattern=paste0(patternR1, ".fastq.gz"), full.names = TRUE))
    fnRs <- sort(list.files(path, pattern=paste0(patternR2, ".fastq.gz"), full.names = TRUE))
   
    print("- Process sample files :")
    print(fnFs)
    print(fnRs)
    
    # Extract sample names, assuming filenames have format: SAMPLE-NAME_R1.fastq
    # sample.names <- sapply(strsplit(basename(filtFs), "_R1"), `[`, 1)
    sample.names <- sapply(strsplit(basename(fnFs), paste0("_", patternR1)), `[`, 1)

    # Chemins d'entree-sortie
    filt_path <- file.path(dadapath, "0_trimmes") # On mettra les reads trimmes dans 0_trimmes/
    filtFs <- file.path(filt_path, paste0(sample.names, paste0("_", patternR1, "_filt.fastq.gz")))
    filtRs <- file.path(filt_path, paste0(sample.names, paste0("_", patternR2, "_filt.fastq.gz")))

    # Examiner la qualite des reads
    print("- Plotting quality profiles :")
    
    plotQualityProfile(fnFs[1:10])
    ggsave(paste0(dadapath, "/0_quality", patternR1, ".png"), device="png")
    print(paste0(dadapath, "/0_quality", patternR1, ".png"))
    
    plotQualityProfile(fnRs[1:10])
    ggsave(paste0(dadapath, "/0_quality", patternR2, ".png"), device="png")
    print(paste0(dadapath, "/0_quality", patternR2, ".png"))

    # Trimming
    print("## Filter and Trim ##")
    if (locus=="18S-V9") {
        trunc_len <- 110
    } else {
	if (locus=="COI") {
            trunc_len <- 200
        }  else {
            trunc_len <- 220
        }
    }
	
    print(paste0("- Truncation length : ", trunc_len))
    out <- filterAndTrim(fnFs, filtFs, fnRs, filtRs, matchIDs=TRUE, maxN=0, maxEE=2, truncQ=2, truncLen=trunc_len, rm.phix=TRUE, verbose=TRUE, multithread=TRUE)
    out <- as.data.frame(out)
    out$sample <- sub("_R1F.fastq.gz|_R2F.fastq.gz", "", rownames(out))
    out <- out[,c(3,1,2)]
    #saveRDS(out, paste0(dadapath, "/1_filterAndTrim_", patternR1, patternR2, ".rds"))
    write.table(out, file=paste0(dadapath, "/1_filterAndTrim_", patternR1, patternR2, ".csv"), sep=";", col.names=T, row.names=F)

    # Mettre a jour les donnees : enlever les echantillons vides
    exists <- file.exists(filtFs) & file.exists(filtRs)
    filtFs <- filtFs[exists]
    filtRs <- filtRs[exists]
    sample.names <- sapply(strsplit(basename(filtFs), paste0("_", patternR1)), `[`, 1)

    # Apprentissage
    print("## Learning Errors ##")
    errF <- learnErrors(filtFs, nbases=1e8, multithread=TRUE, randomize=TRUE)
    errR <- learnErrors(filtRs, nbases=1e8, multithread=TRUE, randomize=TRUE)
    #saveRDS(errF, paste0(dadapath, "/2_err", patternR1, ".rds"))
    #saveRDS(errR, paste0(dadapath, "/2_err", patternR2, ".rds"))
    print(paste0(dadapath, "/2_err", patternR1, ".rds"))
    print(paste0(dadapath, "/2_err", patternR2, ".rds"))

    # Plot des erreurs
    print("- Plotting errors :")
    plotErrors(errF, nominalQ=TRUE)
    ggsave(paste0(dadapath, "/2_err", patternR1, ".png"), device="png")
    plotErrors(errR, nominalQ=TRUE)
    ggsave(paste0(dadapath, "/2_err", patternR2, ".png"), device="png")
    print(paste0(dadapath, "/2_err", patternR1, ".png"))
    print(paste0(dadapath, "/2_err", patternR2, ".png"))

    # Dereplication des sequences identiques. NB: en plus, dada2 fait un consensus (qualite moyenne) des sequences identiques. Le modele va s'en servir.
    print("## Dereplicating identical sequences ## ")
    derepFs <- derepFastq(filtFs, verbose=TRUE)
    derepRs <- derepFastq(filtRs, verbose=TRUE)
    names(derepFs) <- sample.names
    names(derepRs) <- sample.names
    #saveRDS(derepFs, paste0(dadapath, "/3_derep", patternR1, "s.rds"))
    #saveRDS(derepRs, paste0(dadapath, "/3_derep", patternR2, "s.rds"))
    print(paste0(dadapath, "/3_derep", patternR1, "s.rds"))
    print(paste0(dadapath, "/3_derep", patternR2, "s.rds"))

    # Infer the sequence variants in each sample
    print("## ASV inference ## ")
    dadaFs <- dada(derepFs, err=errF, multithread=TRUE)
    dadaRs <- dada(derepRs, err=errR, multithread=TRUE)
    #saveRDS(dadaFs, paste0(dadapath, "/4_dada", patternR1, "s.rds"))
    #saveRDS(dadaRs, paste0(dadapath, "/4_dada", patternR2, "s.rds"))
    print(paste0(dadapath, "/4_dada", patternR1, "s.rds"))
    print(paste0(dadapath, "/4_dada", patternR2, "s.rds"))

    # Contigage des sequences obtenues
    print("## Merging Sequences ## ")
    mergers <- mergePairs(dadaFs, derepFs, dadaRs, derepRs, maxMismatch=0, minOverlap=12, verbose=TRUE)
    #saveRDS(mergers, paste0(dadapath, "/5_contigs", patternR1, patternR2, ".rds"))
    print(paste0(dadapath, "/5_contigs", patternR1, patternR2, ".rds"))

    # Construire une table des sequences
    print("## Making sequence table ## ") 
    seqtab <- makeSequenceTable(mergers)
    #saveRDS(seqtab, paste0(dadapath, "/6_seqtab", patternR1, patternR2, ".rds"))
    print(paste0(dadapath, "/6_seqtab", patternR1, patternR2, ".rds"))

    # Inspecter la distribution de taille des sequences uniques 
    print("- Inspect ASV length distribution : ")
    length_distribution = table(nchar(getSequences(seqtab)))
    #saveRDS(length_distribution, paste0(dadapath, "/7_length_distribution", patternR1, patternR2, ".rds"))
    print(paste0(dadapath, "/7_length_distribution", patternR1, patternR2, ".rds"))

    # Enlever tous les ASV qui sont beaucoup plus grands/petits que la longueur attendue
    print(paste0("- Removing ASV outside length range [", lengthMin, ":", lengthMax, "]"))
    seqtab2 <- seqtab[,nchar(colnames(seqtab)) %in% seq(lengthMin,lengthMax)]
    #saveRDS(seqtab2, paste0(dadapath, "/7_seqtab2", patternR1, patternR2, ".rds"))
    print(paste0(dadapath, "/7_seqtab2", patternR1, patternR2, ".rds"))

    # Enlever les chimeres, last dada step
    print("## Removing chimeras ## ")
    seqtab2.nochim <- removeBimeraDenovo(seqtab2, method="consensus", multithread=TRUE, verbose=TRUE)
    saveRDS(seqtab2.nochim, paste0(dadapath, "/7_nochimeras", patternR1, patternR2, ".rds"))
    print(paste0(dadapath, "/7_nochimeras", patternR1, patternR2, ".rds"))

    # Inspecter les chimeres
    dim(seqtab2)
    dim(seqtab2.nochim)
    sum(seqtab2.nochim)/sum(seqtab2)

    #Check des reads, table finale
    getN <- function(x) sum(getUniques(x))
    print("Creating summary table")
    read.track <- cbind(sample=sample.names, 
                       dada_F=sapply(dadaFs,getN),
                       dada_R=sapply(dadaRs,getN),
                       merged_reads=sapply(mergers, getN), 
                       reads_before_chimera_removal=rowSums(seqtab2),
                       non_chimeric_reads=rowSums(seqtab2.nochim))
    read.track <- merge(out, read.track, by="sample")
    read.track$dada_F <- as.numeric(as.character(read.track$dada_F))
    read.track$dada_R <- as.numeric(as.character(read.track$dada_R))
    read.track$merged_reads <- as.numeric(as.character(read.track$merged_reads))
    read.track$reads_before_chimera_removal <- as.numeric(as.character(read.track$reads_before_chimera_removal))
    read.track$non_chimeric_reads <- as.numeric(as.character(read.track$non_chimeric_reads))
    read.track$final_perc_reads_retained <- round(as.numeric(as.character(read.track$non_chimeric_reads))/as.numeric(as.character(read.track$reads.in))*100, 1) 

    write.table(read.track, file=paste0(dadapath, "/8_readtrack", patternR1, patternR2, ".csv"), sep=";", col.names=T, row.names=F)
    saveRDS(read.track, paste0(dadapath, "/8_readtrack", patternR1, patternR2, ".rds"))

    #save final asv tables
    print("- Saving final ASV table :")
    asvname <- paste0(patternR1, patternR2)
    saveRDS(seqtab2.nochim, paste0(dadapath, "/9_", asvname,"_asv.rds"))
    print(paste0(dadapath, "/9_", asvname,"_asv.rds"))

}

main <- function() {
    # Recuperer les arguments passes au programme
    args = commandArgs(trailingOnly=TRUE)

    # Dossier ou se trouvent les fichiers a traiter
    path = args[1]

    # Dossier ou on va ecrire les fichiers intermediaires de Dada2
    dadapath = args[2]
    lengthMin = args[3]
    lengthMax = args[4]
    ligation = args[5]

    #Foward/reverse reads names
    #default = R1 & R2
    #ligation = R1F & R2R or R2F & R1R
    R1 = args[6]
    R2 = args[7]
    
    #Filter and Trim parameters can be different regarding to the locus
    locus = args[8]
    
    print("### Dada2 parameters ### ")
    print(paste0("- Input Path with fastq files to process : ", path)) 
    print(paste0("- Output Path with dada results : ", dadapath)) 
    print(paste0("- Amplicon min length : ", lengthMin)) 
    print(paste0("- Amplicon max length : ", lengthMax)) 
    print(paste0("- Ligation data (R1FR2R R2FR1R) ? : ", ligation)) 
    print(paste0("- Reads forward pattern : ", R1)) 
    print(paste0("- Reads reverse pattern : ", R2)) 
    print(paste0("- Locus ? : ", locus)) 
    
    #test if input folder exists or exit
    if (!dir.exists(path)) {
        stop("Input folder does not exist") 
    }
    
    #Run dada2
    basicdada(path, dadapath, lengthMin, lengthMax, R1, R2, locus)
}

if (!interactive()) {
        main()
}
