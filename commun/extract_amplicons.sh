#!/usr/bin/env bash

INPUT_SWARM="$1"
INPUT_FASTA="$2"
OUTPUT_FOLDER="$3"

INPUT_FASTA_FORMATTED=$(mktemp -p $SCRATCH)
. /appli/bioinfo/frogs/2.0.0/env.sh

sortAbundancies.py --size-separator ';size=' --input-file ${INPUT_FASTA} --output-file ${INPUT_FASTA_FORMATTED}

. /appli/bioinfo/frogs/2.0.0/delenv.sh

. /appli/bioinfo/muscle/3.8/env.sh
AMPLICONS=$(mktemp -p $SCRATCH)
mkdir -p "${OUTPUT_FOLDER}"
while read OTU ; do
    tr " " "\n" <<< "${OTU}" | sed -e 's/^/>/' > "${AMPLICONS}"
    seed=$(head -n 1 "${AMPLICONS}")
    grep -A 1 -F -f "${AMPLICONS}" "${INPUT_FASTA_FORMATTED}" | \
        sed -e '/^--$/d' > "${OUTPUT_FOLDER}/${seed/>/}.fasta"
    muscle -in "${OUTPUT_FOLDER}/${seed/>/}.fasta" -out "${OUTPUT_FOLDER}/${seed/>/}.aln" > $OUTPUT_FOLDER/muscle.log
done < "${INPUT_SWARM}"
rm "${AMPLICONS}"
rm "${INPUT_FASTA_FORMATTED}"

. /appli/bioinfo/muscle/3.8/delenv.sh
